package de.haslmeierfx.flashlight;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;


public class MainActivity extends AppCompatActivity {
    ImageButton btn;
    private boolean flashStatus = false;
    private Camera camera;
    private Parameters param;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = (ImageButton) findViewById(R.id.button);
    }

    /**Button is pressed*/
    public void onClick(View view){
        boolean flashFeature = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if(flashFeature == false) {
            Toast.makeText(MainActivity.this, "This System does not support Flashlight", Toast.LENGTH_SHORT).show();
        }else {
            if(flashStatus){
                turnFlashOff();
            }else{
                turnFlashOn();
            }
        }

    }

    private void turnFlashOn(){
        //Check if sdk is higher than 23
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CameraManager camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            String cameraId = null;
            try {
                cameraId = camManager.getCameraIdList()[0];
                camManager.setTorchMode(cameraId, true);
            }catch(Exception e){
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "Exception turnFlashOn()", Toast.LENGTH_SHORT).show();
            }
        }else{
            try{
                camera = Camera.open();
                param = camera.getParameters();
                param.setFlashMode(Parameters.FLASH_MODE_TORCH);
                camera.setParameters(param);
                camera.startPreview();
            }catch(Exception e){
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "Exception turnFlashOn()", Toast.LENGTH_SHORT).show();
            }
        }
        btn.setImageResource(R.drawable.on);
        flashStatus = true;
    }
    private void turnFlashOff(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            CameraManager camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            String cameraId = null;
            try {
                cameraId = camManager.getCameraIdList()[0];
                camManager.setTorchMode(cameraId, false);
            }catch(Exception e){
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "Exception turnFlashOFF()", Toast.LENGTH_SHORT).show();
            }
        }else {
            try {
                camera = Camera.open();
                param = camera.getParameters();
                param.setFlashMode(Parameters.FLASH_MODE_OFF);
                camera.setParameters(param);
                camera.stopPreview();
                camera.release();
            }catch(Exception e){
                e.printStackTrace();
                Toast.makeText(getBaseContext(), "Exception turnFlashOff()", Toast.LENGTH_SHORT).show();
            }
        }
        btn.setImageResource(R.drawable.off);
        flashStatus = false;
    }
}
